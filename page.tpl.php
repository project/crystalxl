<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">
<head>
    <title><?php print $head_title ?></title>
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
    
    <meta name="author" content="All: ... [Nazev webu - www.url.cz]; e-mail: info@url.cz" />
    <meta name="copyright" content="Design/Code: Vit Dlouhy [Nuvio - www.nuvio.cz]; e-mail: vit.dlouhy@nuvio.cz" />
    
    <title>CrystalXL</title>
    <meta name="description" content="..." />
    <meta name="keywords" content="..." />

</head>

<body id="www-url-cz">

<!-- Main -->
<div id="main" class="box">

    <!-- Header -->
    <div id="header">

        <!-- Logotyp -->
        <h1 id="logo">
          <?php if ($site_name) : ?>
            <div class='site-name'><a href="<?php print($base_path) ?>" title="<?php print t('Home') ?>"><?php print($site_name) ?></a></div>
          <?php endif;?>
          <?php if ($site_slogan) : ?>
            <div class='site-slogan'><?php print($site_slogan) ?></div>
          <?php endif;?>
        </h1>
        <hr class="noscreen" />          

        <!-- Quick links -->
        <div class="noscreen noprint">
            <p><em>Quick links: <a href="#content">content</a>, <a href="#tabs">navigation</a>, <a href="#search">search</a>.</em></p>
            <hr />
        </div>

        <!-- Search -->
        <div id="search" class="noprint">
            <form action="" method="get">
                <fieldset><legend>Search</legend>
                    <label><span class="noscreen">Find:</span>
                    <span id="search-input-out"><input type="text" name="" id="search-input" size="30" /></span></label>
                    <input type="image" src="<?php print base_path() . path_to_theme() ?>/design/search_submit.gif" id="search-submit" value="OK" />
                </fieldset>
            </form>
        </div> <!-- /search -->

    </div> <!-- /header -->

     <!-- Main menu (tabs) -->
     <div id="tabs" class="noprint">
        <h3 class="noscreen">Navigation</h3>
        
        <?php if (count($primary_links)) : ?>
          <ul class="box">
               <?php foreach ($primary_links as $link): ?>
                   <?php if (request_uri()=='/'.$link['href'] || (request_uri()=='/' && $link['href']=='<front>' )): ?>
                      <li id="active">
                   <?php else: ?>
                      <li>
                   <?php endif; ?>
                   <?php
                   $href = $link['href'] == "<front>" ? base_path() : base_path() . $link['href'];
                   print "<a href='" . $href . "'>" . $link['title'] . "<span class='tab-l'></span><span class='tab-r'></span></a>";
                   ?>
                   </li>
               <?php endforeach; ?>
          </ul>
        <?php endif; ?> 
            
        <hr class="noscreen" />
     </div> <!-- /tabs -->

    <!-- Page (2 columns) -->
    <div id="page" class="box">
    <div id="page-in" class="box">

        <div id="strip" class="box noprint">

            <!-- RSS feeds -->
            <p id="rss"><strong>RSS:</strong> <a href="#">articles</a> / <a href="#">comments</a></p>
            <hr class="noscreen" />

            <!-- Breadcrumbs -->
            <div class="breadcrumb">You are here: <?php print $breadcrumb ?><strong><?php print $title ?></strong></div>
            <hr class="noscreen" />
            
            
        </div>
        <!-- /strip -->
        
        <div id="content">
            <?php if ($help != ""): ?>
                <div id="help"><?php print $help ?></div>
            <?php endif; ?>
    
            <?php if ($messages != ""): ?>
              <?php print $messages ?>
            <?php endif; ?>
            
            <?php if ($tabs != ""): ?>
              <div class="tabs"><?php print $tabs ?></div>
            <?php endif; ?>
            
            <?php print $content; ?>   
        </div>

         

        <!-- /content -->

        <!-- Right column -->
        <div id="col" class="noprint">
            <div id="col-in">
                <ul id="category">
                  <?php if ($sidebar_right != ""): ?>
                    <td id="sidebar-right">
                      <?php print $sidebar_right ?>
                    </td>
                    <?php endif; ?>
                </ul>
                <hr class="noscreen" />
            </div> <!-- /col-in -->
        </div> <!-- /col -->

    </div> <!-- /page-in -->
    </div> <!-- /page -->

    <!-- Footer -->
    <?php if ($footer_message) : ?>
      <div id="footer">
        <div id="top" class="noprint"><p><span class="noscreen">Back on top</span> <a href="#header" title="Back on top ^">^<span></span></a></p></div>
          <hr class="noscreen" />
          <p id="copyright"><?php print $footer_message;?></p>
        </div>
    <?php endif; ?>
    <!-- /footer -->
    
     

</div> <!-- /main -->
<?php print $closure;?>
</body>
</html>