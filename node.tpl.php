<div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
  <div class="article">
    <?php print $picture ?>
    
    <?php if ($page) { ?>
      <h2><span><?php print $title ?></span></h2>
      <p><?php print $content ?></p>
      <p class="info noprint">
        <span class="submitted"><?php print $submitted ?></span><span class="noscreen"></span>
        <span class="taxonomy"><?php print $terms ?></span><span class="noscreen"></span>
        <?php if ($links): ?>
           <span class="user"><?php print $links ?></a></span><span class="noscreen"></span>
        <?php endif; ?>
      </p>
    <?php } elseif ($teaser) { ?>
      <h2><span><a href="<?php print $node_url ?>"><?php print $title ?></a></span></h2>
      <p class="info noprint">
        <span class="submitted"><?php print $submitted ?></span><span class="noscreen"></span>
        <span class="taxonomy"><?php print $terms ?></span><span class="noscreen"></span>
        <?php if ($links): ?>
          <span class="user"><?php print $links ?></a></span><span class="noscreen"></span>
        <?php endif; ?>
      </p>
      <p><?php print $content ?></p>
    <?php } ?>
    
  </div>
  <hr class="noscreen" />
</div>