<?php
function phptemplate_breadcrumb($breadcrumb) {
  $sep = ' &gt; ';
  if (count($breadcrumb) > 0) {
    return implode($breadcrumb, $sep) . $sep;
  }
  else {
    return t("Home");
  }
}